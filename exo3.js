/**
 * Log the object with the given message
 *
 * @param {string} message
 */
function myPrint (msg) {
    console.log(this.username + ': ' + msg);
}

function myBind(fn, thisArg, args){
    fn.call(thisArg, args);
}

myBind(myPrint, {username: "Tom"}, "Hello!");

// Expected output
// -> Tom: Hello!
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

const toDownload = {
    'file1': 'https://outils.blgconnect.fr/public/text_file_1.txt',
    'file2': 'https://outils.blgconnect.fr/public/text_file_2.txt',
    'file3': 'https://outils.blgconnect.fr/public/this_file_does_not_exist'
};

/** Choice 1: async via Promise **/

/**
 * Download a list of files and return the promise to the result once all downloads are completed
 * (whether they are successful or not).
 *
 * need to install xmlhttprequest $npm install xmlhttprequest
 *
 * @param {Object} fileList
 * @return {Promise<Object>}
 */

function downloadList (fileList) {
    let result={};

    return new Promise(function (resolve, reject) {
        for (const file in fileList) {

            /*resolve.get( fileList[file], function(data){
                $('donnee').html( data );
            }, Text );

            result[file]=donnee;*/
        }


        if(result==null){reject("error")}
        else {resolve(result)}
        })

}

downloadList(toDownload).then(result => console.log(result));

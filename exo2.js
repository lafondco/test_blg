/**
 * Set any data at the given dot-separated path in destObj.
 *
 * @param {Object} destObj
 * @param {string} pathStr
 * @param {*} data
 */
function setDataAtPath (destObj, pathStr, data) {

  const words = pathStr.split('.'); // on récupère un tableau ou on entre dans chaque colone un des mots du chemin pour accéder à la data
  const tabLength=words.length; //taille du tableau

  let newObj={};


  if (tabLength===1) {
    destObj[pathStr] = data;
  }else{

    newObj[words[tabLength-1]] = data
    for (let i=tabLength-2; i>0; i--){
      newObj[words[i]]=newObj
    }
    destObj[words[0]] = newObj;
  }
}

const obj = {
  'age': 28,
  'name': 'Richard',
  'address': {
    'street': 'rue de la Sous Préfecture',
    'locality': 'Compiègne'
  }
};


setDataAtPath(obj, 'gender', 'male');
setDataAtPath(obj, 'job.type', 'Software developer');
setDataAtPath(obj, 'address.country', 'France');

//Problème avec cette méthode on éfface l'ancien chanp adresse
console.log(obj);